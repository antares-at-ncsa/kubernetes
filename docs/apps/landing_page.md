Landing page
============

The Antares at NCSA landing page at https://antares.ncsa.illinois.edu/ is powered by the `landing-page` app. The container image is automatically built and pushed to the container registry using the GitLab CI system (see the `gitlab-ci.yaml` file) upon committing changes in the `/src/landing-page` path to the `main` branch.
