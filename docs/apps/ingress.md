Ingress Control
============================

Connecting external HTTP requests to the services running in Kubernetes in a high-availability (HA) manner requires several components, including a load balancer and an ingress controller.

[Traefik](https://doc.traefik.io/traefik) ingress controller
-----------------------------------------------------------

The ingress controller is Traefik v2. It is configured as a LoadBalancer service, bound to the static internal IP address controlled by MetalLB that is associated with the external floating IP address provisioned by Radiant (i.e. OpenStack) and assigned to our DNS records. 

[MetalLB](https://metallb.universe.tf/) load balancer
---------------------------------------------------------

The top half of the schematic below illustrates how to think about it from the other direction. Inbound requests to `antares.ncsa.illinois.edu` resolve to the external IP address `141.142.x.y` due to the DNS records we request of the NCSA networking staff. The external IP address is associated in OpenStack with the MetalLB-controlled internal network IP address `192.168.a.b`.

In the event the worker node running Traefik goes down, Kubernetes will respawn Traefik on another worker node. MetalLB, which itself runs as a DaemonSet on all worker nodes, will notice this event and will reassign the internal IP address `192.168.a.b` to this new worker node running Traefik. This is illustrated by the bottom half of the figure.

SSL [Certificate Manager](https://cert-manager.io/docs/)
--------------------------------------------------------

Although Traefik provides automatic TLS certificate services, it cannot do this in HA mode (meaning no more than a single replica of Traefik can be online). To support HA, we use `cert-manager` to obtain and automatically renew TLS certificates from LetsEncrypt for our HTTPS web service ingresses.

Ingress system schematic
--------------------------------------------

![](k8s_ingress.png)
