Bootstrap the Kubernetes cluster
------------------------------------------

Overview
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The ANTARES Kubernetes cluster is constructed by Terraform from a `dedicated repository <https://gitlab.com/manning-ncsa/antares-terraform>`_. This repository constitutes a `Terraform module <https://www.terraform.io/docs/configuration/files/>`_ designed to bootstrap a fully functioning Kubernetes cluster on the `NCSA Radiant <radiant.ncsa.illinois.edu/>`_ cloud computing platform, using `GitLab as a Terraform remote backend <https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html>`_ to capture the state of the infrastructure.

Step-by-step instructions and notes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create new GitLab API token at https://gitlab.com/-/profile/personal_access_tokens.

Create new Rancher API token at https://gonzo-rancher.ncsa.illinois.edu/apikeys.

Terraform provides remote backends. GitLab uses the `HTTP remote backend <https://www.terraform.io/docs/backends/types/http.html>`_ as detailed in `GitLab managed Terraform State <https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html>`_.

Example::

    terraform {
        backend "http" {
            address = "http://myrest.api.com/foo"
            lock_address = "http://myrest.api.com/foo"
            unlock_address = "http://myrest.api.com/foo"
        }
    }

The Terraform repo https://gitlab.com/manning-ncsa/antares-terraform acts as a "`module <https://www.terraform.io/docs/configuration/files/>`_":

    A module is a collection of .tf and/or .tf.json files kept together in a directory.

    A Terraform module only consists of the top-level configuration files in a directory; nested directories are treated as completely separate modules, and are not automatically included in the configuration.

    Terraform evaluates all of the configuration files in a module, effectively treating the entire module as a single document. **Separating various blocks into different files is purely for the convenience of readers and maintainers, and has no effect on the module's behavior**.

`Terraform Providers <https://www.terraform.io/docs/configuration/blocks/providers/index.html>`_ enable Terraform to control resources associated with that provider. For example, `the OpenStack provider <https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs>`_ is used to create/edit/destroy VMs in Radiant.

Ensure Helm v3.2.1 (or later) is installed and stable repo is updated::

    helm repo rm stable
    helm repo add stable https://charts.helm.sh/stable
    helm repo update

Install Terraform from https://www.terraform.io/downloads.html and extract to your favorite `bin` folder::

    $ terraform version
    Terraform v0.14.0

Create a file for secrets needed in the environment configuration::

    cat > gitlab.secrets.env << EOF
    export GITLAB_PROJECT_ID="123"
    export GITLAB_USERNAME="username"
    export GITLAB_API_TOKEN="token"
    export CLUSTER_STATE_NAME="antares"
    export OPENSTACK_USERNAME="username"
    export OPENSTACK_PASSWORD="password"
    EOF

The ``terraform init`` command is used to initialize a working directory containing Terraform configuration files. (`Documentation link <https://www.terraform.io/docs/commands/init.html>`_) ::

    $ source gitlab.secrets.env
    $ terraform init \
      -backend-config="address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${CLUSTER_STATE_NAME}" \
      -backend-config="lock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${CLUSTER_STATE_NAME}/lock" \
      -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${CLUSTER_STATE_NAME}/lock" \
      -backend-config="username=${GITLAB_USERNAME}" \
      -backend-config="password=${GITLAB_API_TOKEN}" \
      -backend-config="lock_method=POST" \
      -backend-config="unlock_method=DELETE" \
      -backend-config="retry_wait_min=5"

    Initializing the backend...

    Successfully configured the backend "http"! Terraform will automatically
    use this backend unless the backend configuration changes.

    Initializing provider plugins...
    - Finding hashicorp/local versions matching "1.4.0"...
    - Finding latest version of terraform-provider-openstack/openstack...
    - Finding latest version of rancher/rancher2...
    - Finding latest version of hashicorp/helm...
    - Installing hashicorp/local v1.4.0...
    - Installed hashicorp/local v1.4.0 (signed by HashiCorp)
    - Installing terraform-provider-openstack/openstack v1.33.0...
    - Installed terraform-provider-openstack/openstack v1.33.0 (self-signed, key ID 4F80527A391BEFD2)
    - Installing rancher/rancher2 v1.10.6...
    - Installed rancher/rancher2 v1.10.6 (signed by a HashiCorp partner, key ID 2EEB0F9AD44A135C)
    - Installing hashicorp/helm v1.3.2...
    - Installed hashicorp/helm v1.3.2 (signed by HashiCorp)

    Partner and community providers are signed by their developers.
    If you'd like to know more about provider signing, you can read about it here:
    https://www.terraform.io/docs/plugins/signing.html

    Terraform has created a lock file .terraform.lock.hcl to record the provider
    selections it made above. Include this file in your version control repository
    so that Terraform can guarantee to make the same selections by default when
    you run "terraform init" in the future.

    Terraform has been successfully initialized!

    You may now begin working with Terraform. Try running "terraform plan" to see
    any changes that are required for your infrastructure. All Terraform commands
    should now work.

    If you ever set or change modules or backend configuration for Terraform,
    rerun this command to reinitialize your working directory. If you forget, other
    commands will detect it and remind you to do so if necessary.

Run ``terraform plan`` to preview changes to be made::

    $ ./tf antares plan -var openstack_username="${OPENSTACK_USERNAME}" -var openstack_password="${OPENSTACK_PASSWORD}"

    Acquiring state lock. This may take a few moments...
    openstack_networking_router_v2.kube_router: Refreshing state... [id=e2535c40-cbcb-4dea-bd41-1c20726fbe72]

    An execution plan has been generated and is shown below.
    Resource actions are indicated with the following symbols:
    + create

    Terraform will perform the following actions:

    # local_file.initialize will be created
    + resource "local_file" "initialize" {
        + content              = (known after apply)
        + directory_permission = "0755"
        + file_permission      = "0755"
        + filename             = "./antares-init/initialize.sh"
        + id                   = (known after apply)
        }

    ...

    # rancher2_cluster_sync.kube will be created
    + resource "rancher2_cluster_sync" "kube" {
        + cluster_id         = (known after apply)
        + default_project_id = (known after apply)
        + id                 = (known after apply)
        + kube_config        = (sensitive value)
        + state_confirm      = 1
        + synced             = true
        + system_project_id  = (known after apply)
        + wait_monitoring    = false
        }

    Plan: 34 to add, 0 to change, 0 to destroy.


Run the ``terraform apply`` to bootstrap the cluster::

    $ ./tf antares apply -var openstack_username="${OPENSTACK_USERNAME}" -var openstack_password="${OPENSTACK_PASSWORD}"

After a few minutes the VMs and associated networking resources should be created and configured in Radiant, and the Rancher web interface should show the Kubernetes cluster construction. The ANTARES Terraform plan generates a local folder ``antares-init``::

    $ ll antares-init/
    total 36
    drwxrwxr-x 6 manninga manninga 4096 Dec  8 09:48 ..
    -rw------- 1 manninga manninga  195 Dec  8 09:51 nfs-condo.yaml
    -rw------- 1 manninga manninga 1675 Dec  8 09:51 antares.pem
    -rw------- 1 manninga manninga  230 Dec  8 09:51 metallb-config.yaml
    -rw------- 1 manninga manninga  705 Dec  8 09:51 traefik.yaml
    -rw------- 1 manninga manninga  467 Dec  8 09:51 traefik-dashboard.yaml
    -rwxr-xr-x 1 manninga manninga 1684 Dec  8 09:51 initialize.sh
    -rw-r--r-- 1 manninga manninga  955 Dec  8 09:51 README
    drwxr-xr-x 2 manninga manninga 4096 Dec  8 09:51 .

The ``README`` file provides essential information including where the generated SSH keys are stored ``$HOME/.ssh/antares.pem`` and how to use them to connect to the Kubernetes control-plane::

    $ export KUBECONFIG=/home/manninga/.kube/antares.kubeconfig

    $ kubectl get node
    NAME                       STATUS   ROLES               AGE   VERSION
    antares-controlplane-0   Ready    controlplane,etcd   17m   v1.19.4
    antares-worker-0   Ready    worker              15m   v1.19.4
    antares-worker-1   Ready    worker              15m   v1.19.4

Run the initialization script to install Traefik, MetalLB, and Longhorn components::

    $ ./antares-init/initialize.sh
    # install longhorn nfs
    deployment.apps/longhorn-nfs-provisioner created

    # Install nfs storage client for condo
    WARNING: This chart is deprecated
    Release "nfs-condo" has been upgraded. Happy Helming!
    NAME: nfs-condo
    LAST DEPLOYED: Tue Dec  8 12:04:50 2020
    NAMESPACE: nfs-condo
    STATUS: deployed
    REVISION: 2
    TEST SUITE: None

    # Installing metallb
    podsecuritypolicy.policy/controller configured
    podsecuritypolicy.policy/speaker configured

    # Install traefik on 192.168.2.125 / 141.142.217.114
    WARNING: This chart is deprecated
    Release "traefik" has been upgraded. Happy Helming!
    NAME: traefik
    LAST DEPLOYED: Tue Dec  8 12:04:57 2020
    NAMESPACE: traefik
    STATUS: deployed
    REVISION: 2
    TEST SUITE: None
    NOTES:
    1. Get Traefik's load balancer IP/hostname:

        NOTE: It may take a few minutes for this to become available.

        You can watch the status by running:

            $ kubectl get svc traefik --namespace traefik -w

        Once 'EXTERNAL-IP' is no longer '<pending>':

            $ kubectl describe svc traefik --namespace traefik | grep Ingress | awk '{print $3}'

    2. Configure DNS records corresponding to Kubernetes ingress resources to point to the load balancer IP/hostname found in step 1


        # ----------------------------------------------------------------------
        # OPENSTACK SERVERS
        # ----------------------------------------------------------------------
        openstack private key = /home/manninga/.ssh/antares.pem"

        # antares-controlplane-0
        ssh -i /home/manninga/.ssh/antares.pem centos@141.142.216.88

        # ----------------------------------------------------------------------
        # TRAEFIK
        # ----------------------------------------------------------------------
        floating ip for traefik = 141.142.217.114
        dashboard for traefik   = https://traefik.141.142.217.114.xip.io/traefik/

        # ----------------------------------------------------------------------
        # KUBERNETES
        # ----------------------------------------------------------------------
        kubeconfig = /home/manninga/.kube/antares.kubeconfig
        export KUBECONFIG=\"/home/manninga/.kube/antares.kubeconfig\"

    ~/work/src/manning-ncsa/antares-terraform
    $


Configuration Notes
^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::  This configuration has been automatically applied by the Terraform script since `commit b48eb9642deca61bfa3f13781662123c539bf1a6 <https://gitlab.com/antares-at-ncsa/terraform/-/commit/b48eb9642deca61bfa3f13781662123c539bf1a6>`_.

There has been an issue where the control-plane node crashed due to reaching storage capacity when log files grew without bound. To prevent this in the future, we manually created a Docker ``/etc/docker/daemon.json`` on each control-plane node::

   [centos@antares-controlplane-0 ~]$ sudo -i

   [root@antares-controlplane-0 ~]# cat >> /etc/docker/daemon.json << EOF
   > {
   >   "log-driver": "json-file",
   >   "log-opts": {
   >     "max-size": "10m",
   >     "max-file": "3"
   >   }
   > }
   > EOF

   [root@antares-controlplane-0 ~]# systemctl restart docker

   [root@antares-controlplane-0 ~]# logout


Recreate the cluster without losing floating IP addresses
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once DNS records have been created and static public IP addresses have been provisioned, it is undesirab

1. Manually remove nodes from OpenStack.
2. Manually remove cluster from Rancher.
3. Refresh Terraform state (this will fail) 

::

   $ export CLUSTER_STATE_NAME="cluster-name"
   $ export OPENSTACK_USERNAME="username"
   $ export OPENSTACK_PASSWORD="password"
   $ ./tf ${CLUSTER_STATE_NAME} refresh \
      -var openstack_username="${OPENSTACK_USERNAME}" \
      -var openstack_password="${OPENSTACK_PASSWORD}"
    
      Acquiring state lock. This may take a few moments…
      local_file.traefik_dashboard[0]: Refreshing state…
      ...
      Refreshing state… [id=83de20be27bcf42a58b8707aa0b9c5f447ac9b8b]
      Releasing state lock. This may take a few moments...

4. Show the current Terraform state. 

::

   $ terraform state list

   data.openstack_identity_auth_scope_v3.scope
   data.openstack_networking_network_v2.ext_net 
   ...
   openstack_networking_secgroup_v2.cluster_security_group
   openstack_networking_subnet_v2.cluster_subnet 

5. Manually remove any lingering Rancher components. Example:

::

   $ terraform state rm \
      rancher2_cluster.kube \
      rancher2_cluster_sync.kube \
      'rancher2_app_v2.monitor[0]' \
      'rancher2_app_v2.longhorn-system[0]'

6. Reapply the Terraform state to reconstruct the cluster.

::

   $./tf ${CLUSTER_STATE_NAME} apply \
      -var openstack_username="${OPENSTACK_USERNAME}" \
      -var openstack_password="${OPENSTACK_PASSWORD}"

Mount large fast storage volumes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Manually create three 200MB volumes in Radiant and attach them to each of the k8s worker nodes. SSH into each worker node and partition, format, and add the volumes to ``/etc/fstab``. Use the internal network IP of the worker node and SSH from a terminal session in one of the control-plane nodes.

:: 

    $ ssh -i /home/manninga/.ssh/antares.pem centos@141.142.219.136
    Last login: Fri Jan 22 20:27:50 2021 from 140.177.234.164

    [centos@antares-controlplane-0 ~]$ ssh 192.168.2.16
    The authenticity of host '192.168.2.16 (192.168.2.16)' can't be established.
    ECDSA key fingerprint is SHA256:TM4tkTvpHQBNBuezDrT2YpvdxdDoGGPjshk88mwO+HY.
    ECDSA key fingerprint is MD5:9d:3e:dc:ee:0e:c1:b1:8b:02:48:1c:90:94:18:77:10.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added '192.168.2.16' (ECDSA) to the list of known hosts.

    [centos@antares-worker-0 ~]$ sudo -i
    
    [root@antares-worker-0 ~]# parted /dev/vdb mklabel gpt
    Information: You may need to update /etc/fstab.

    [root@antares-worker-0 ~]# parted -a opt /dev/vdb mkpart primary xfs 0% 100%
    Information: You may need to update /etc/fstab.

    [root@antares-worker-0 ~]# lsblk                                
    NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
    sr0     11:0    1  492K  0 rom  
    vda    253:0    0   40G  0 disk 
    └─vda1 253:1    0   40G  0 part /
    vdb    253:16   0  200G  0 disk 
    └─vdb1 253:17   0  200G  0 part 

    [root@antares-worker-0 ~]# mkfs.xfs /dev/vdb1
    meta-data=/dev/vdb1              isize=512    agcount=4, agsize=13107072 blks
            =                       sectsz=512   attr=2, projid32bit=1
            =                       crc=1        finobt=0, sparse=0
    data     =                       bsize=4096   blocks=52428288, imaxpct=25
            =                       sunit=0      swidth=0 blks
    naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
    log      =internal log           bsize=4096   blocks=25599, version=2
            =                       sectsz=512   sunit=0 blks, lazy-count=1
    realtime =none                   extsz=4096   blocks=0, rtextents=0

    [root@antares-worker-0 ~]# echo '/dev/vdb1 /mnt/longhorn xfs defaults,noatime,discard,nofail 0 2' >> /etc/fstab

    [root@antares-worker-0 ~]# mkdir /mnt/longhorn
    
    [root@antares-worker-0 ~]# mount /mnt/longhorn
