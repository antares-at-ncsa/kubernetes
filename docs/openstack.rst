OpenStack CLI
===========================================

Install the OpenStack CLI
--------------------------------------------
::

  $ pip install python-openstackclient

Initialize OpenStack CLI
--------------------------------------------

Download the ``$PROJECT-openrc.sh`` file via the web interface. Source this file and then run commands::

  $ source ~/Downloads/bbdr-openrc.sh 
  Please enter your OpenStack Password for project bbdr as user manninga: *****

  $ openstack floating ip list
  +--------------------------------------+---------------------+------------------+--------------------------------------+--------------------------------------+----------------------------------+
  | ID                                   | Floating IP Address | Fixed IP Address | Port                                 | Floating Network                     | Project                          |
  +--------------------------------------+---------------------+------------------+--------------------------------------+--------------------------------------+----------------------------------+
  | ad14d54f-efff-4b5e-998c-8ec7a0776d32 | 141.142.216.56      | 192.168.2.36     | b32b1c20-2062-472c-aa1e-58761bf67944 | 4ac37a6f-6c39-4104-824d-374e0b751b49 | dedb408d1b0a4aa0acdbf08180e4069e |
  | fb7d6d07-6b87-4311-abc9-50f8533218b9 | 141.142.217.114     | 192.168.2.125    | 179fa043-4ecf-44f2-b811-4f9563b53e00 | 4ac37a6f-6c39-4104-824d-374e0b751b49 | dedb408d1b0a4aa0acdbf08180e4069e |
  +--------------------------------------+---------------------+------------------+--------------------------------------+--------------------------------------+----------------------------------+

Node login via SSH
----------------------------------------

::

  $ openstack server ssh --address-type ext-net --login centos --identity ~/.ssh/antares.pem antares-controlplane-0

