Deployment Infrastructure
============================================

* `NCSA Radiant <https://radiant.ncsa.illinois.edu/>`_
* Kubernetes cluster
* Rancher
* Helm
* Terraform

.. image:: terraform_schematic.png
    
Radiant
-------------------------------------------

The `NCSA Radiant elastic computing service <https://wiki.ncsa.illinois.edu/display/ICI/Radiant>`_ will provide the initial hardware required for service development and deployment. 

.. toctree::
   :maxdepth: 2

   radiant_request

Rancher and Harbor
-------------------------------------------

Rob Kooper maintains a `Rancher instance at NCSA <https://gonzo-rancher.ncsa.illinois.edu/>`_ that we can use to provision and manage the k8s cluster nodes. This provides a robust set of tools for managing the cluster, including authentication and authorization for team members, cluster state snapshots, rolling upgrades for Kubernetes components (e.g. kube-apiserver, kubelet, etc.) 

Rob also maintains a `Harbor instance at NCSA <https://hub.ncsa.illinois.edu/>`_ for a private image registry, providing a resource suited to development where public access to images may not be desirable. We also use the container registry available from GitLab.com. 

Terraform
-------------------------------------------

We use Terraform to create and synchronize our infrastructure, allowing us to bootstrap a Kubernetes cluster automatically and reproducibly.


ArgoCD
-------------------------------------

ArgoCD is a continuous deployment system that supports the `GitOps paradigm <https://www.gitops.tech/>`_.

.. admonition:: GitOps in a nutshell

  GitOps is a way of implementing Continuous Deployment for cloud native applications. It focuses on a developer-centric experience when operating infrastructure, by using tools developers are already familiar with, including Git and Continuous Deployment tools.

  The core idea of GitOps is having a Git repository that always contains declarative descriptions of the infrastructure currently desired in the production environment and an automated process to make the production environment match the described state in the repository. If you want to deploy a new application or update an existing one, you only need to update the repository - the automated process handles everything else. It’s like having cruise control for managing your applications in production.

This `excellent tutorial <https://www.arthurkoziel.com/setting-up-argocd-with-helm/>`_ is what we followed to deploy ArgoCD. The private repo access was provided manually via the ArgoCD web UI by first generating a repo_read API token on GitLab and using it as the password in the Repository settings.

Authentication
^^^^^^^^^^^^^^^^

Following `this documentation <https://argoproj.github.io/argo-cd/operator-manual/user-management/#dex>`_, GitLab has been added as an identity provider for authentication to the ArgoCD sevice. The GitLab Oauth application definition was created through the admin's personal GitLab account. 

Members of the `antares-at-ncsa` GitLab group can use the "Log in via GitLab" button at https://antares.ncsa.illinois.edu/argo-cd


App structure
^^^^^^^^^^^^^^^^^^^^

The goal is to capture the complete state of the deployed services in a reproducible and revertable way by following the GitOps methodology. The combination of ArgoCD and a deployment git repo allows in most cases for apps to be developed and deployed without requiring `kubectl` access to the Kubernetes cluster directly.

There are infrastructure level deployments, including

- ingress controllers
- vault system

that are either deployed manually via Helm (ingress controllers) or require manual intervention to start (initializing or unsealing the vault after the vault pod is restarted). All other deployments and resources should be under ArgoCD control via the git repo.

.. warning::
  Before including ArgoCD as one of the apps under the root app that ArgoCD is controlling (i.e. allowing ArgoCD to manage itself), first deploy the Vault system and ensure that the private deployment git repo access credentials are in place.

Command line access
^^^^^^^^^^^^^^^^^^^^^^

To use the local `ArgoCD CLI <https://argoproj.github.io/argo-cd/getting_started/#2-download-argo-cd-cli>`_, use the more complex login command, which will open a browser window to complete the GitHub authentication:

```
argocd login antares.ncsa.illinois.edu --grpc-web-root-path /argo-cd --sso
```
  
Persistent Volumes for Storage
-------------------------------------

Storage is provided by `NCSA Condo <https://wiki.ncsa.illinois.edu>`_, which is a GPFS (General Parallel File System) cluster housed in dedicated racks at `NPCF <http://www.ncsa.illinois.edu/about/facilities/npcf>`_. Two storage options:

* flash storage == SSD fast, expensive
* condo storage == HDD slower, cheap ($53/TB/year)

The PersistentVolume (PV) Kubernetes resources are provided by two primary storage classes: ``longhorn`` and ``nfs-condo`` (default). 

NFS Condo
^^^^^^^^^^^^^^^^^^^^^^^^

The default ``nfs-condo`` storage class allocates storage from the Condo cluster, which persists independently of the k8s cluster. To provision storage for a deployed app that will be easy to recover even if the k8s cluster is destroyed and recreated, you should manually create a PV/PVC pair like in this example::

  ---
  apiVersion: v1
  kind: PersistentVolume
  metadata:
    name: nextcloud-data-pv
    labels:
      name: nextcloud-data
  spec:
    storageClassName: nfs-condo
    capacity:
      storage: 50Gi
    accessModes:
      - ReadWriteMany
    persistentVolumeReclaimPolicy: Retain
    nfs:
      path: /taiga/ncsa/radiant/bbgl/cluster-antares/nextcloud/nextcloud-data
      server: taiga-nfs.ncsa.illinois.edu
      readOnly: false
  ---
  apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: nextcloud-data-pvc
    namespace: nextcloud
  spec:
    storageClassName: nfs-condo
    accessModes:
      - ReadWriteMany
    resources:
      requests:
        storage: 50Gi
    selector:
      matchLabels:
        name: nextcloud-data

The actual files can be accessed by opening a terminal in one of the control-plane nodes and mounting the NFS::

  $ ssh -i /home/manninga/.ssh/antares.pem centos@141.142.219.136

  [root@antares-controlplane-0 ~]# mount taiga-nfs.ncsa.illinois.edu:/radiant/projects/bbdr /mnt/condo

  [root@antares-controlplane-0 ~]# ls -l /mnt/condo/antares/
  cassandra-cassandra-data-cassandra-4-pvc-eec861f9-1a43-45af-8408-775f69b9432f/
  landing-page-landing-page-data-pvc-aa139bb8-b778-4e6a-928c-bbd1177461a8/
  nextcloud/
  ...
  
Longhorn
^^^^^^^^^^^^^^^^^^^^^^^^

The ``longhorn`` storage class is provided by the `Longhorn <https://rancher.com/products/longhorn/>`_ system, which allocates space from the k8s nodes themselves (i.e. Radiant VM instance disk space) to provision the PVs claimed by the k8s pods. This storage is fast (suitable for MySQL databases for example) and is replicated across several nodes for resilience, but if all the nodes are destroyed, then the data is lost. Backups can be configured in the `Longhorn management interface at a URL similar to this <https://gonzo-rancher.ncsa.illinois.edu/k8s/clusters/c-g42rs/api/v1/namespaces/longhorn-system/services/http:longhorn-frontend:80/proxy/#/setting>`_.


After mounting the Condo filesystem as described above, create a backup folder like so::

  $ mkdir /mnt/condo/backup

Then in the Longhorn settings, set the backup root location to (replace ``bbdr`` with your project code) ::

  nfs://taiga-nfs.ncsa.illinois.edu:/radiant/projects/bbdr/backup
