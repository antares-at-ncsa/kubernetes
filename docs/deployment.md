Deployment repo structure
==========================

Our deployment repo follows the GitOps paradigm and has a structure with three top-level folders
```
/src/
/charts/
/apps/
```
- `src` contains the source code for services and associated Dockerfiles for creating and pushing images,
- `charts` contains the Helm charts for deploying services, and
- `apps` contains the ArgoCD application (app) definitions.

Apps are composed of one or more services. The top level app is called
`root` and is defined in `/apps/root/`; it actually controls all the
other apps (recursively...turtles all the way down). This is how we
declare all the apps we want to be running at any given moment on the
Kubernetes cluster.

As an example for how this works, take the `landing-page` app that
powers <https://antares.ncsa.illinois.edu/>. To get this online, we add
an ArgoCD app definition file in
`/apps/root/templates/app-landing-page.yaml`. This tells ArgoCD to
create an app based on the Helm chart defined in
`/charts/landing-page/`.

This is where the Helm charts for the various services come into play.
In the case of this simple `landing-page` app, there is only one service
(with one corresponding Helm chart) involved, so there is only one
definition file `/apps/landing-page/templates/landing-page.yaml`.
However, you can imagine that there can be multiple services required
for different apps, such as our Vault-based secret management system
that involves the services Vault, Consul, and Vault Secrets Operator.

The real magic here is that **all we have to do to change what is
deployed on the cluster is to make a commit to this deployment repo and
ArgoCD notices the change and applies it**. ArgoCD also sports a
user-friendly web GUI that we may choose to expose via Ingress at some
point; in the meantime it is accessible via `kubectl port-forward`.

Infrastructure-level ArgoCD applications
----------------------------------------

There are many infrastructure-level applications that we deploy via
ArgoCD (including ArgoCD itself!):

- argo-cd
- traefik
- metallb
- cert-manager
- sealed-secrets
- longhorn
- nfs-condo
