Job Management System
============================================

Job submission context
---------------------------------------------

The context in which users submit jobs involves several considerations:

How do we authenticate users?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The JupyterHub system already provides authn/authz. If we restrict job API access to Jupyter notebooks then we have the access control we need. By not providing external access to the job API via an ingress, it may be sufficient to allow any pod with internal Kubernetes network access to the job API service to use it. 

What is the environment in which the code executes and how is it defined?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The jobs will by definition run as Kubernetes Jobs and hence in containers defined by 

* a tagged Docker image
* environment variables
* mounted volumes

For the first iteration of the job system, we should run the same image and environment as the Jupyter server. This will ensure that the interactive development environment of the Jupyter notebook will run the same when launched as a job.

How does the user provide the code to be executed?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There should be a standard volume mount for all jobs, providing a way for the user to provide input data and capture job output as necessary. JupyterHub already creates a PersistentVolume (PV) for each user, so it may be sufficient to mount this same PVC to the job container such that files residing anywhere in the user's home folder ``/home/jovyan`` are accessible to the job. Clearly there would need to be conventions about how to specify input and output folders for the job.

How are running jobs monitored and managed?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There will need to be a job API endpoint like ``/status`` that returns the status of all or specified jobs, as well as a ``/delete`` endpoint. We can leverage existing frameworks and tools, such as the UWS pattern for the API structure and the uws-client python package

* `Universal Worker Service Pattern <https://www.ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html>`_
* `uws-client Python package <https://github.com/aipescience/uws-client/>`_

Job definition
---------------------------------------------

Job definitions will be submitted in a YAML format specified similar to Kubernetes resource definitions (not all fields are provided by the user)::

  apiVersion: v1
  kind: Job
  #
  # Identifying and tracking information about the job
  #
  metadata:
    name: my-job
    user: bob
    labels: []
    created: 2021-02-18T19:14:20
    id: 3027fbe2a6ff446735c42ec8b512c7cf
  spec:
    #
    # Container image to run. This will likely not be customizable.
    # 
    image:
      name: registry.gitlab.com/antares-at-ncsa/antares/jupyterhub
      tag: latest
    #
    # Environment in which to run the code. These would be additional environment variables to
    # those already included in the Jupyter notebook container.
    #
    environment:
    - name: ENV_VAR_1
      value: "apples"
    - name: ENV_VAR_2
      value: "bananas"
    #
    # The working directory is relative to the home folder for the container user (e.g. `/home/jovyan`).
    # The job can read/write to any system folder accessible to it, but anything modified outside the 
    # home folder will be lost when the job completes and the container is deleted.
    #
    workdir: "jobs/my-job"
    command: "python job_script.py"
    #
    # How many parallel Kubernetes Jobs to spawn
    #
    replicas: 1
    