Antares at NCSA
===========================================

ANTARES is an alert-broker system developed by the National Optical-Infrared Astronomy Research Laboratory (NOIRLab). ANTARES is currently processing ZTF alert data—annotating them using external catalogs, providing customizable filters to identify subsets of events, and distributing alerts to the astronomy community near real time. The design of the ANTARES system is based on a parallel stream-processing architecture: it ingests alerts from input Apache Kafka 5 streams, processes them in parallel using containerized Python packages, stores the results in an Apache Cassandra​ 6 database, and produces streams of output/filtered alerts again using Kafka. 

Antares at NCSA will integrate ANTARES with alert streams from LIGO/Virgo, as well the IceCube neutrino observatory in Antarctica, thereby extending its role as a multi-messenger science tool. We will use signal processing, outlier detection and deep learning approaches to identify exotic sources such as kilonovae and neutron star-black hole mergers. We will also use the NCSA-deployed ANTARES to stress-test against alert streams at the scale of LSST.   

In the upcoming LSST era, ANTARES will be the nexus that enables near real-time investigation and follow-up of a wide range of time-domain astrophysical phenomena including pulsating and variable stars, gravitational lensing events, and supernovae.

For more information visit [the Antares at NCSA website](https://antares.ncsa.illinois.edu/) and [our documentation](https://antares-at-ncsa.gitlab.io/kubernetes).
